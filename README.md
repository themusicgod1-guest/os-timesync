`os-timesync` can help you check whether NTP time sync is enabled in OS settings.
It is not that reliable.

Note that the result does not tell you whether the local time is accurate.

```js
var timesync = require("os-timesync");

timesync.checkEnabled(function (err, enabled) {
    if (err) {
        console.log("can't check: " + err.toString());
        return;
    }
    console.log("enabled =", enabled);
});
```

